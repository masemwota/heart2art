package edu.sjsu.android.heart2art;

import android.os.Bundle;
import android.view.ViewGroup;
import android.view.View;
import android.view.*;

import androidx.fragment.app.Fragment;

public class DrawingFragment extends Fragment {

    private View view;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_draw, container, false);
        return view;
    }
}
