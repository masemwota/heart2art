package edu.sjsu.android.heart2art;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import com.parse.ParseUser;

/**
     Page shows more about the user - name, description, and theme (black or white)
*/

public class ProfileFragment extends Fragment {
    private View view;
    String name;
    String desc;
    Boolean theme;



    public void getUserDetails() 
    {
        ParseUser currentUser = ParseUser.getCurrentUser();

        name = currentUser.getString("username");
        desc = currentUser.getString("desc");
        theme = (Boolean) currentUser.get("theme");

        EditText ut = view.findViewById(R.id.userText);
        ut.setText(name);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user, container, false);
        getUserDetails();
        return view;
    }
}