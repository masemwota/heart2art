package edu.sjsu.android.heart2art;

import android.graphics.Path;

/**
 * Class that contains information about the user's continuous line of art.
 * It will be used to create a single object for each continuous line the user draws with their finger
 */
public class FingerDraw {

    public int color;
    public boolean embossEffect;
    public boolean blurEffect; // If the user has specified a blur mode or not
    public int strokeWidth;    // The width specified from User to draw with their finger
    public Path path;          // Represents the path drawn from the User

    /**
     * Contructor used to initialize a User's finger path on the screen
     *
     * @param color - Color of user's path
     * @param stampEffect - User's emboss mode specification
     * @param blurEffect - User's blue mode specification
     * @param strokeWidth - User's specified path width
     * @param path - Path object of user's finger on screen
     */
    public FingerDraw(int color, boolean embossEffect, boolean blurEffect, int strokeWidth, Path path){
        this.color = color;
        this.embossEffect = embossEffect;
        this.blurEffect = blurEffect;
        this.strokeWidth = strokeWidth;
        this.path = path;
    }
}
