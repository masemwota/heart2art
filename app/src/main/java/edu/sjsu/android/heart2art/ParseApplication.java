package edu.sjsu.android.heart2art;

import android.app.Application;

import com.parse.Parse;

public class ParseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
//        ParseObject.registerSubclass(Post.class);
//        ParseObject.registerSubclass(User.class);
        // set applicationId, and server server based on the values in the Heroku settings.
        // clientKey is not needed unless explicitly configured
        // any network interceptors must be added with the Configuration Builder given this syntax
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId("heart2art") // should correspond to APP_ID env variable
                .clientKey("CS175TeamProject")  // set explicitly unless clientKey is explicitly configured on Parse server
                .server("https://heart2art.herokuapp.com/parse").build());

    }
}